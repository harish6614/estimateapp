/**
 * 
 *
 *      +-----------------------------------------------------------------------------------------------------------------+ 
 *      |                                             Copyright - NWMSU                                                   |
 *      |                                           NWMSU INTERNAL USE ONLY                                               |
 *      |-----------------------------------------------------------------------------------------------------------------|                    |
 *      |Unit 08 : Chinta Raja Srikar Karthik                                                                             |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This controller program calculates and passes processed data to statistics view                     |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                        |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Chinta Raja Srikar Karthik       0.0.1(Initial)       Initial code added to develop statistics controller.       |
 *      |                                                                                                                 |             
 *      |                                                                                                                 |
 *      +-----------------------------------------------------------------------------------------------------------------+
 */
const express = require('express')
const api = express.Router()
const Model = require('../models/estimate.js')
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'estimates'
const HashMap = require('hashmap')
const passport = require('../config/passportConfig.js')


// RESPOND WITH VIEWS  --------------------------------------------



// GET stats
api.get('/', passport.isAuthenticated, (req, res) => {
    const materialMap = new HashMap();
    const miscellaneousMap = new HashMap();
    const userCostMap = new HashMap();
    const estimates = req.app.locals.estimates

    var totalMaterialCostForAllEstimates = 0;
    for (var i = estimates.query.length - 1; i >= 0; i--) {
        estimate = estimates.query[i]
        var totalLabCost = 0,
            totalMaterialCost = 0,    
            totalCostPerMaterial = 0,
            totalCost = 0,
            totalMiscellaneousCost = 0,
            totalHousingCost=0,
            totalFoodCost=0,
            totalTransCost=0;
        console.log("----------------------------------------------")
        console.log("Estimate Name:" + estimate.name)
        for (var j = 0; j < estimate.materials.length; j++) {
            let material = estimate.materials[j]
            totalCostPerMaterial = (estimate.squareFeet / material.coverageSquareFeetPerUnit) * material.unitcost
            totalMaterialCost += totalCostPerMaterial
            totalMaterialCostForAllEstimates += totalCostPerMaterial
            console.log("Material Name:" + material.product)
            console.log("Total cost per material:" + totalCostPerMaterial)
            let tempName = material.product && material.product.length ? (material.product).charAt(0).toUpperCase() + (material.product).slice(1) : "";
            // console.log(tempName)
            if (materialMap.has(tempName)) {
                let data = materialMap.get(tempName)
                let count = totalCostPerMaterial
                materialMap.set(tempName, data + count)
            } else {
                materialMap.set(tempName, totalCostPerMaterial);
            }
        } 
        console.log("Total Material cost for this estimate: " + totalMaterialCost)
        totalLabCost = estimate.laborDollarsPerHour * estimate.numberOfPeople * estimate.hoursWorkedPerDay * estimate.numberOfDays
        console.log("Total Labour Cost: "+totalLabCost);
        totalHousingCost = estimate.numberOfPeople*estimate.numberHotelNights*estimate.hotelDollarsPerNight
        console.log("Total Housing Cost: "+totalHousingCost)
        totalFoodCost = estimate.numberOfPeople*estimate.foodDollarsPerDay*estimate.numberOfDays
        console.log("Total Food Cost: "+totalFoodCost)
        totalTransCost = estimate.numberOfVehicles*estimate.milesPerVehicle*estimate.dollarsPerMile 
        console.log("Total Transportation cost: "+totalTransCost)
        for (var j = 0; j < estimate.miscellaneous.length; j++) {
            let miscellaneous = estimate.miscellaneous[j]
            totalMiscellaneousCost += miscellaneous.cost
        }
        console.log("Total Miscellaneous cost: "+totalMiscellaneousCost)
        totalCost = totalMaterialCost + totalLabCost+totalHousingCost+totalFoodCost+totalTransCost+totalMiscellaneousCost
        let tempEstimateName = (estimate.name).charAt(0).toUpperCase() + (estimate.name).slice(1)
        userCostMap.set(tempEstimateName, totalCost)

    }
    // totCost = totMatCost + totLaborCost + totHousingCost + totFoodCost
    // + totTrnsCost + totMisCost 

    materialMap.forEach(function (value, key) {
        console.log("totalMaterialCostForAllEstimates:" + totalMaterialCostForAllEstimates)
        console.log(key + " :" + materialMap.get(key))
        let percent = Math.max(Math.round(((value / totalMaterialCostForAllEstimates) * 100) * 10) / 10, 2.8).toFixed(2)
        materialMap.set(key, percent)
        console.log(key + " :" + materialMap.get(key))
    })

    res.render('estimate/statistics.ejs', {
        map: materialMap,
        userCostMap: userCostMap
        // age:age
    })
})





// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

module.exports = api