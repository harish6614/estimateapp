# README #

Welcome to CGE! This is our attempt to create a project for a client ...

### What is this repository for? ###

* CGE is an app. to generate cost estimates -- it is used for 44-563
* This is version 0.0.1

### How do I get set up? ###

* Clone the repo
* That's it (there's nothing else to do at this time)

### Contribution guidelines ###

* Remember, pull early and often
* Comment your code
* Problems? Don't panic!

### Who do I talk to? ###

* Talk to any of the GAs or instructors if you have problems.
* All answers are available on stackoverflow.com ;-)

### Miscellaneous

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* A common problem? https://stackoverflow.com/questions/44888049/how-to-git-pull-while-ignoring-the-local-changes